# Instructions
Make sure you have docker installed.

## Usage
To start, run:
```bash
$ cd Docker
$ make up
```

Navigate to http://localhost:8080 to see confirmation of a succcessful launch.

To see running containers, run:
```bash
$ make status
```

To stop, run:
```bash
$ make down
```
