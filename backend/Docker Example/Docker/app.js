const express = require("express");
const app = express();

app.get("*", (req, res) => {
    res.send("The server is listening.");
});

const port = process.env.PORT || 8080;

app.listen(port, () => {
    console.log(`The server is listening on port ${port}.`);
});
